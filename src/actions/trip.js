import {
    GET_TRIPS_SUCCESS,
    GET_TRIPS_FAIL
} from "../constants";
import { trip as tripApi } from "../api";


export const list = params => async dispatch => {
    const payload = await tripApi.list(params);

    try {
        dispatch({ type: GET_TRIPS_SUCCESS, payload });
    } catch (error) {
        dispatch({
            type  : GET_TRIPS_FAIL,
            error : payload
        });
    }
};
