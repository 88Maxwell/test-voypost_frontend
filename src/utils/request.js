import queryString from "query-string";
import { apiPrefix, apiUrl } from "../../configs";

class API {
    constructor(apiURL, prefix = "api/v1") {
        this.prefix = prefix;
        this.apiUrl = apiURL;
        this.token = "";
        [ "get", "post", "patch", "delete" ].forEach(
            method =>
                (this[method] = async (url, data, params) =>
                    this.request({
                        method : method.toUpperCase(),
                        body   : data,
                        url,
                        params
                    }))
        );
    }

    async request({ url, method, body, params = {} }) {
        const query = Object.keys(params).length ? `?${queryString.stringify(params)}` : "";
        const response = await fetch(`${this.apiUrl}/${this.prefix}${url}${query}`, {
            method,
            withCredentials : true,
            crossDomain     : true,
            body            : method !== "GET" ? body : undefined
        });

        const json = await response.json();

        if (json.status !== 200) throw json.error;

        return json.data;
    }
}

export default new API(apiUrl, apiPrefix);
