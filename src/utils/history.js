import { createBrowserHistory } from "history";
import qhistory from "qhistory";
import { parse, stringify } from "query-string";

export default qhistory(createBrowserHistory(), stringify, parse);
