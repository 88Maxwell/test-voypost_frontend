import {
    GET_TRIPS_SUCCESS,
    GET_TRIPS_FAIL
} from "../constants";

const initialState = {
    trips           : [],
    error           : "",
    fromPointSearch : "",
    toPointSearch   : ""
};

export default (state = initialState, { type, payload = {}, error }) => {
    switch (type) {
        case GET_TRIPS_SUCCESS:
            return {
                ...state,
                trips : payload
            };

        case GET_TRIPS_FAIL:
            return { ...state, error };

        default:
            return state;
    }
};
