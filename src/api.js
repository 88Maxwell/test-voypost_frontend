import request from "./utils/request";

export const trip = {
    list : params => request.get("/trips", null, params)
};
