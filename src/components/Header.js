import React, { Component } from "react";

import { connect } from "react-redux";
import PropTypes from "prop-types";

import * as TripActions from "../actions/trip";
import SearchForm from "./forms/Search";
import style from "./header.module.css";

class Header extends Component {
    static propTypes = {
        list : PropTypes.func.isRequired
    };

    searchHandler = data => async evt => {
        evt.stopPropagation();
        evt.preventDefault();

        await this.props.list(data);
    };

    render() {
        return (
            <header className={style.header}>
                <h1>Voypost Studio Test Task</h1>
                <br />
                <SearchForm handleSubmit={this.searchHandler} />
            </header>
        );
    }
}

export default connect(null, TripActions)(Header);
