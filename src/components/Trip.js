import React, { Component } from "react";
import PropTypes from "prop-types";

import style from "./trip.module.css";

class Trip extends Component {
    static propTypes = {
        data : PropTypes.object.isRequired
    };

    render() {
        const { fromPoint, toPoint, description, vehicle, departDate } = this.props.data;
        const date = new Date(departDate);
        const dateString = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;

        return (
            <div className={style.tripContainer}>
                <ul className={style.trip}>
                    <li className={style.tripItem}>
                        <div>from point</div>
                        <div>{fromPoint}</div>
                    </li>
                    <li className={style.tripItem}>
                        <div>to point</div>
                        <div>{toPoint}</div>
                    </li>
                    <li className={style.tripItem}>
                        <div>description</div>
                        <div>{description}</div>
                    </li>
                    <li className={style.tripItem}>
                        <div>depart date</div>
                        <div>{dateString}</div>
                    </li>
                    <li className={style.tripItem}>
                        <div>vehicle</div>
                        <div>{vehicle}</div>
                    </li>
                </ul>
            </div>
        );
    }
}

export default Trip;
