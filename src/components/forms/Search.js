import React, { Component } from "react";
import PropTypes from "prop-types";
import style from "./search.module.css";

export default class SearchForm extends Component {
    static propTypes = {
        handleSubmit : PropTypes.func.isRequired
    }

    state = {
        from : "",
        to   : ""
    }

    handleChange = param => e => this.setState({ [param]: e.target.value })

    render() {
        const { from, to } = this.state;
        const { handleSubmit } = this.props;

        return (
            <form className={style.form} onSubmit={handleSubmit(this.state)}>
                <div className={style.inputContainer}>
                    <div className={style.input}>
                        <label htmlFor="from">From:</label>
                        <input
                            id="from" type="text" onChange={this.handleChange("from")}
                            value={from}
                        />
                    </div>
                    <div className={style.input}>
                        <label htmlFor="to">To:</label>
                        <input
                            id="to" type="text" onChange={this.handleChange("to")}
                            value={to}
                        />
                    </div>
                </div>
                <button type="submit">Search</button>
            </form>
        );
    }
}
