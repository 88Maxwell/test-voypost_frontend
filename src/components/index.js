/* eslint-disable camelcase */
import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import * as TripActions from "../actions/trip";
import Trip from "./Trip";

import style from "./index.module.css";
import Header from "./Header";

class Main extends Component {
    static propTypes = {
        trips : PropTypes.array,
        list  : PropTypes.func.isRequired
    };

    async componentDidMount() {
        await this.props.list();
    }

    render() {
        const { trips } = this.props;

        return (
            <div className={style.container}>
                <Header />
                <main className={style.main}>
                    <ul>
                        {trips && trips.length ? (
                            trips.map(tripData => <Trip key={tripData.id} data={tripData} />)
                        ) : (
                            <li className={style.noTripsItem} >There are no trips matching the search criteria</li>
                        )}
                    </ul>
                </main>
            </div>
        );
    }
}

export default connect(
    state => ({ trips: state.trips.trips }),
    TripActions
)(Main);
